// Reference: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date

// UNIX EPOCH: 
//0 = Jan 1st 1970 00:00:00 am
//1000 = Jan 1st 1970 00:00:01 am
//-1000 = Dec 31st 1969 11:59:59 pm

// get a timestamp....
var rawDate = new Date();
console.log('rawDate = ', rawDate);
// get 0 index month value, Jan = 0
console.log('rawDate = ', rawDate.getMonth());

// Reference: https://momentjs.com/

const moment = require('moment');

var momentDate = new moment();

// DISPLAY FORMATTING
console.log('momentDate = ', momentDate);
console.log('momentDate.format(MMM) = ', momentDate.format('MMM'));
console.log('momentDate.format(MMM YYY) = ', momentDate.format('MMM YYY'));
console.log('momentDate.format(MMM YYY) = ', momentDate.format('MMM Do, YYYY'));

// MANIPULATION
momentDate.add(100, 'year');
console.log('momentDate after add = ', momentDate.format('MMM Do, YYYY'));

momentDate.subtract(9, 'months')
console.log('momentDate after subtract = ', momentDate.format('MMM Do, YYYY'));

momentDate.add(100, 'year').subtract(9, 'months');
console.log('momentDate after operation chaining = ', momentDate.format('MMM Do, YYYY'));