const expect = require('expect');
var {
  generateMessage,
  generateLocationMessage
} = require('./message');

describe('generateMessage', () => {
  it('should generate a correct message object', () => {
    var from = 'Diana';
    var text = 'Hola';
    var message = generateMessage(from, text);

    expect(typeof message.createdAt).toBe('number');
    expect(message.from).toBe(from);
    expect(message.text).toBe(text);
  });
});

describe('generateLocationMessage', () => {
  it('should generate a correct location message', () => {
    var from = 'Diana';
    var latitude = '10';
    var longitude = '-84';
    var locationMessage = generateLocationMessage(from, latitude, longitude);

    expect(typeof locationMessage.createdAt).toBe('number');
    expect(locationMessage.from).toBe(from);
    expect(locationMessage.url).toBe(`https://www.google.com/maps?q=${latitude},${longitude}`);
  });
});
