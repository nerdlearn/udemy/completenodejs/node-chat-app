const expect = require('expect');
var {
  isRealString
} = require('./validation');

describe('isRealString', () => {
  var validString = '1234';
  var emptySpacesString = '      '; 
  var notAString = 1234; 
  
  var isRealStringResult;
  
  it('should return false if parameter is not a string', () => {
	isRealStringResult = isRealString(notAString);
    expect(isRealStringResult).toBe(false);
  }); 
  
  it('should return false if string is only spaces', () => {
	isRealStringResult = isRealString(emptySpacesString);
    expect(isRealStringResult).toBe(false);
  }); 
  
  it('should return true if string is valid', () => {
	isRealStringResult = isRealString(validString);
    expect(isRealStringResult).toBe(true);
  });
});
