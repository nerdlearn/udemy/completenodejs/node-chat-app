require('./config/config.js');

const path = require('path');
const http = require('http');
const express = require('express');
const socketIO = require('socket.io');
const {
  generateMessage,
  generateLocationMessage
} = require('./utils/message');

const {
  isRealString
} = require('./utils/validation');

var {
  Users
} = require('./utils/users');

const publicPath = path.join(__dirname, '../public');
const port = process.env.PORT;

var app = express();
var server = http.createServer(app);
var io = socketIO(server);
var users = new Users();

app.use(express.static(publicPath));

io.on('connection', (socket) => {
  console.log('New user connected');

  socket.on('join', (params, callback) => {
    if (!isRealString(params.name) || !isRealString(params.room)) {
      return callback('Name and room name are both required');
    }

    socket.join(params.room);
    //remove user from previous rooms
    users.removeUser(socket.id);

    //add user to the room list
    users.addUser(socket.id, params.name, params.room);

    //socket.leave() to exit

    //send to ALL in a room
    //io.emit -> io.to('room name string').emit

    //send to ALL in a room except current user
    //socket.broadcast.emit ->socekt.broadcast.to('room name string').emit

    //send to a single socket
    //socket.emit -> socket.emit :)

    io.to(params.room).emit('updateUsersList', users.getUserList(params.room));

    //send to a single socket
    socket.emit('newMessage', generateMessage('Admin', 'Welcome to the chat app'));
    //send to ALL in a room except current user
    socket.broadcast.to(params.room).emit('newMessage', generateMessage('Admin', `${params.name} has joined`));

    callback();
  });

  socket.on('createMessage', (message, callback) => {
    console.log('createMessage', message);

    //io.emit sends to ALL sockets
    /*
    io.emit('newMessage', {
    from: message.from,
    text: message.text,
    createdAt: new Date().getTime()
    });

     */
    //socket.broadcast.emit sends to ALL sockets except mine
    //socket.broadcast.emit('newMessage', generateMessage(message.from, message.text));
    user = users.getUser(socket.id);

    if (user && isRealString(message.text)) {
      io.to(user.room).emit('newMessage', generateMessage(user.name, message.text));
    } else{
		console.log('We are not sending this message');
	}

    if (callback) {
      callback();
    }
  });

  socket.on('createLocationMessage', (coords) => {
    user = users.getUser(socket.id);
    if (user) {
      io.to(user.room).emit('newLocationMessage', generateLocationMessage(user.name, coords.latitude, coords.longitude));
    }
  });

  socket.on('disconnect', () => {
    //remove user from previous rooms
    var user = users.removeUser(socket.id);
    if (user) {
      io.to(user.room).emit('updateUsersList', users.getUserList(user.room));
      io.to(user.room).emit('newMessage', generateMessage('Admin', `User ${user.name} has left the chat room.`));
    }
  });
});

server.listen(port, () => {
  console.log('Server listening on port: ', port);
});

module.exports = {
  app
};
