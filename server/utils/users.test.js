const expect = require('expect');
var {
  Users
} = require('./users');

describe('Users', () => {

  var users;

  beforeEach(() => {
    users = new Users();
    users.users = [{
        id: 11,
        name: 'Pedro',
        room: 'MyRoom1'
      }, {
        id: 22,
        name: 'Diana',
        room: 'MyRoom2'
      }, {
        id: 33,
        name: 'Rulo',
        room: 'MyRoom1'
      }
    ];
  });

  it('should add a new user', () => {
    var users2 = new Users();
    var user = {
      id: '123',
      name: 'Pedro',
      room: 'myRoom'
    }
    var resUser = users2.addUser(user.id, user.name, user.room);

    expect(users2.users).toEqual([user]);
  });
  
  it('should remove a user with a correct ID', () => {
	var userBeforeDeletion = users.users[0];
	var userReceived = users.removeUser(userBeforeDeletion.id);
    expect(userReceived).toEqual(userBeforeDeletion);
	expect(users.users.length).toBe(2);
  });
  
  it('should not remove a user with an incorrect ID', () => {
	var userReceived = users.removeUser(345);
    expect(userReceived).toBeUndefined();
	expect(users.users.length).toBe(3);
  });
  
   it('should return a user with a correct ID', () => {
	var userReceived = users.getUser(users.users[0].id);
    expect(userReceived).toEqual(users.users[0]);
  });
  
   it('should not return a user with an incorrect ID', () => {
	var userReceived = users.getUser(443);
    expect(userReceived).toBeUndefined();
  });
  
  it('should return names for MyRoom1', () => {
    var userList = users.getUserList('MyRoom1');
    expect(userList).toEqual(['Pedro','Rulo']);
  });
  
  it('should return names for MyRoom2', () => {
    var userList = users.getUserList('MyRoom2');
    expect(userList).toEqual(['Diana']);
  });
});
