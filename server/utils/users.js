class Users {
  constructor() {
    this.users = [];
  }
  addUser(id, name, room) {
    var user = {
      id,
      name,
      room
    };
    this.users.push(user);
    return user;
  }

  removeUser(id) {
    //return user that was removed;
    var user = this.users.filter((user) => user.id === id)[0];
	
	if(user){
		this.users = this.users.filter((user) => user.id !== id);
	}
    return user;
  }

  getUser(id) {
    var user = this.users.filter((user) => user.id === id)[0];
    return user;
  }

  getUserList(room) {
    var users = this.users.filter((user) => user.room === room);
    var namesArray = users.map((user) => user.name);
    return namesArray;
  }
};

module.exports = {
  Users
};
/*
// ES6 Classes

class Person {
constructor(name, age){
this.name = name;
this.age = age;
this.age = age;
}
getUserDescription(){
return `${this.name} is ${this.age} years old.`;
}

}

var me = new Person('Pedro', 40);

 */
/*

//PRE ES6 CLASSES
[{
id: 'w#4sfrwerewr',//socket ID
name:'Pedro', // user login name
room: 'MyChat Room' // room where they are
}]

var users = [];

var addUser = (id, name, room) => {
var user = {
id, name, room
};

users.push(user);
};

var removeUser = (id) => {

};

var getUser = (id) => {

};

var getUserList = (room) => {

};

model.exports = {
addUser,
removeUser,
getUser,
getUserList
};

*/
